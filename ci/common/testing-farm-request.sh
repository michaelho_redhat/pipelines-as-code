#!/bin/bash

set -e

dnf install -y jq

ENDPOINT="https://api.dev.testing-farm.io/v0.1/requests"

CS_VERSION="${CS_VERSION:-8}"
if [[ "${ARCH}" == "x86_64" ]]; then
    COMPOSE="CentOS-Stream-${CS_VERSION}"
else
    COMPOSE="CentOS-Stream-${CS_VERSION}-aarch64"
fi

# Compose the query for calling the Testing Farm API
cat <<EOF > request.json
{
  "api_key": "${TF_API_KEY}",
  "test": {
    "fmf": {
    "url": "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code",
    "ref": "${REF}",
    "name": "${TMT_PLAN}"
    }
  },
  "environments": [
   {
   "arch": "${ARCH}",
   "os": {"compose": "${COMPOSE}"},
   "variables": {
     "ARCH": "${ARCH}",
     "UUID": "${UUID}",
     "REPO_URL": "${REPO_URL}",
     "REVISION": "${REVISION}",
     "PREPROCESSOR_FILE": "${PREPROCESSOR_FILE}",
     "CS_VERSION": "${CS_VERSION}",
     "TF_SSH_KEY": "${TF_SSH_KEY}"
     },
   "secrets": {
     "AWS_ACCESS_KEY_ID": "${AWS_ACCESS_KEY_ID}",
     "AWS_SECRET_ACCESS_KEY": "${AWS_SECRET_ACCESS_KEY}",
     "REGION": "${AWS_REGION}",
     "BUCKET": "${AWS_BUCKET_REPOS}"
     }
   }
   ]
}
EOF

# Do the API query to Testing Farm with the data from above
curl --silent ${ENDPOINT} \
    --header "Content-Type: application/json" \
    --data @request.json \
    --output response.json

# Show the response, but hide the secrets values
jq 'del(.environments |. [] | .secrets)' response.json

ID=$(jq -r '.id' response.json)
echo "Wait until the job finished at the Testing Farm"
while true; do
    rm -f response.json
    curl --silent --output response.json "${ENDPOINT}/${ID}"
    STATUS=$(jq -r '.state' response.json)
    if [[ "$STATUS" == "complete" ]] || [[ "$STATUS" == "error" ]]; then
        echo ; echo "Finished"
        break
    fi
    echo -n "."
    sleep 30
done

# Check the tests result
RESULT=$(jq -r '.result.overall' response.json)
echo "Result: $RESULT"

# If the result is an error, there is no report to show
# Error means that something was wrong with the call, not that the tests failed
if [[ "$RESULT" == "error" ]]; then
    jq -r '.result.summary' response.json
    exit 1
fi
EXIT_CODE=1
if [[ "$RESULT" == "passed" ]]; then
    EXIT_CODE=0
fi

# Show the URL with the tests report
URL=$(jq -r '.run.artifacts' response.json)
curl --silent --output report.html "$URL/"
echo "The build results are here: $URL"
echo -n "$URL" > URL

exit $EXIT_CODE

