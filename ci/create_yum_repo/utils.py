import logging
import subprocess

util_logger = logging.getLogger("manifests.Utils")

def run_cmd(cmd: list) -> int:
    cp = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    if cp.returncode != 0:
        util_logger.error(
            f"Command {cmd} failed exit code: {cp.returncode}\n"
            f"Command {cmd} args: {cp.args}\n"
            f"Command {cmd} stdout: {cp.stdout}\n"
            f"Command {cmd} stderr: {cp.stderr}\n"
        )
        return 1
    else:
        util_logger.info(
            f"Successfully run the command {cmd}\n"
            f"Command {cmd} stdout: {cp.stdout}\n"
        )

    return 0
