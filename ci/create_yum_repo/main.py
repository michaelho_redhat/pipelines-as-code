#!/usr/bin/env python3

import logging
import logging.config
import os
import sys
import json
import tempfile
from concurrent import futures
from pathlib import Path

import shutil
import boto3
import requests
from jinja2 import Template
from requests.adapters import HTTPAdapter

import utils


def upload_directory(directory: str, prefix: str, bucket: str) -> int:
    ret = 0
    boto3_session = boto3.Session()
    s3 = boto3_session.client("s3")

    input_dir = Path(directory)

    s3_parallel_request_rate = 16
    with futures.ThreadPoolExecutor(s3_parallel_request_rate) as executor:
        upload_task = {}

        for file in input_dir.rglob("*"):
            if not file.is_file():
                continue
            upload_task[
                executor.submit(
                    s3.upload_file,
                        Filename=str(file.absolute()),
                        Bucket=bucket,
                        Key=prefix + "/" + str(file.relative_to(input_dir)),
                        ExtraArgs={"ACL": "public-read"},
                )
            ] = file

        for task in futures.as_completed(upload_task):
            try:
                task.result()
            except Exception as e:
                logging.error(
                    f"Exception {e} encountered while uploading file"
                    f"{upload_task[task]}"
                )
                ret = 1
    return ret


def create_repo(repodir: str) -> int:
    return utils.run_cmd(cmd=["createrepo_c", repodir])


def test_repo(baseurl_prefix: str, centos_version: int, arch: str) -> int:
    """Call dnf makecache to test the created repos are valid"""
    logging.info(f"Testing dnf repo at {baseurl_prefix} for {centos_version},{arch}")

    # Configure and call dnf using a custom dnf.conf to avoid contaminating the host system
    dnfconf_template_file = Path(__file__).parent / "dnf.conf.j2"
    repofile_template_file = Path(__file__).parent / "yumrepo.j2"

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        (tmpdir / "cache").mkdir(parents=True)
        (tmpdir / "logs").mkdir(parents=True)
        (tmpdir / "persist").mkdir(parents=True)
        (tmpdir / "repos").mkdir(parents=True)

        dnfconf = Template(dnfconf_template_file.read_text()).render(tmpdir=tmpdirname)
        (tmpdir / "dnf.conf").write_text(dnfconf)

        repofile = Template(repofile_template_file.read_text()).render(
            centos_version=centos_version, arch=arch, baseurl_prefix=baseurl_prefix
        )
        (tmpdir / "repos" / "automotive.repo").write_text(repofile)
        logging.info(repofile)

        result = utils.run_cmd(
            cmd=[
                "dnf",
                "-v",
                "-c",
                f"{tmpdirname}/dnf.conf",
                "makecache",
                "--repo=auto-*",
            ]
        )
        return result


def test_aws_repo(
    bucket: str, region: str, upload_prefix: str, centos_version: int, arch: str
) -> int:
    """Call dnf makecache to test the uploaded repos are valid"""
    baseurl_prefix = f"https://{bucket}.s3.{region}.amazonaws.com/{upload_prefix}"

    return test_repo(baseurl_prefix, centos_version, arch)


def test_local_repo(repo_dir: str, centos_version: int, arch: str) -> int:
    """Call dnf makecache to test the local repos are valid"""
    baseurl_prefix = f"file://{repo_dir}"

    return test_repo(baseurl_prefix, centos_version, arch)


def download_package(
    package: str,
    output_dir: str,
    mirrors: list,
    repos: list,
    package_arch: str,
    http_session: requests.Session,
) -> int:
    supported_arches = {
        "x86_64": ["x86_64", "i686", "noarch"],
        "aarch64": ["aarch64", "noarch"],
    }

    for url in mirrors:
        for repo in repos:
            for arch in supported_arches[package_arch]:
                package_filename = f"{package}.{arch}.rpm"
                resp = http_session.get(
                    url=(
                        f"{url}{repo}/" f"{package_arch}/os/Packages/{package_filename}"
                    ),
                    allow_redirects=True,
                    timeout=10,
                )
                if resp.status_code == 200:
                    # workaround: S3 doesn't like the character '+' at the URL (PATH or file names)
                    # It's a known issue:
                    # https://stackoverflow.com/questions/36734171/how-to-decide-if-the-filename-has-a-plus-sign-in-it
                    # The next line will rename the files with '+' with '-'. This is done before the
                    # 'createrepo_c' indexes the packages, so the repo's metadata will point
                    # to the files with the new names. dnf/yum and the osbuild can now download
                    # and install the packages.
                    package_filename = package_filename.replace("+", "-")
                    with open(
                        f"{output_dir}/{repo}/{package_arch}/os/{package_filename}",
                        "wb",
                    ) as package_file:
                        package_file.write(resp.content)
                    return 0

    logging.error(
        f"{package} was not found, URL: {url}, REPO: {repo}, ARCH: {package_arch}"
    )
    return 1


def download_packages(
    packages_list_dir: str,
    centos_version: str,
    output_dir: str,
    arch: str,
    repos: list,
    mirrors: list,
) -> int:
    returnCode = 0

    # Create clean output directory structure
    # Failure prone so try to return nice human readable errors
    output_path = Path(output_dir)
    try:
        output_path.mkdir(parents=True, exist_ok=True)
    except Exception as e:
        logging.error("Error: Cannot create/write to %s", output_path)
        logging.error("Error: %s", str(e))
        return 1

    for repo in repos:
        repo_path = output_path / repo / arch / "os"
        if repo_path.exists():
            logging.info("Cleaning output directory: %s", repo_path)
            try:
                shutil.rmtree(repo_path)
            except OSError as e:
                logging.error("Error: cannot delete %s", repo_path)
                logging.error("Error: %s", str(e))
                return 1

        try:
            repo_path.mkdir(parents=True)
        except Exception as e:
            logging.error("Error: Cannot create/write to %s", repo_path)
            logging.error("Error: %s", str(e))
            return 1

    with open(
        f"{packages_list_dir}/{centos_version}-image-manifest.json", "r"
    ) as packages_file:
        json_data = json.load(packages_file)

    packages = (
        json_data[f"{centos_version}"]["arch"][arch]
        + json_data[f"{centos_version}"]["common"]
    )

    http_session = requests.Session()
    http_session.mount("http://", HTTPAdapter(max_retries=10))
    for package in packages:
        package = package.strip()
        x = download_package(package, output_dir, mirrors, repos, arch, http_session)
        if x != 0:
            returnCode = 1

    return returnCode


def main() -> int:
    log_config_path = Path(
        os.path.join(Path(__file__).parent.absolute(), "logging.conf")
    )

    logging.config.fileConfig(log_config_path)
    logging.getLogger("manifests")
    logging.info(":: create_yum_repo started ::")

    supported_arches = ["aarch64", "x86_64"]
    repositories = ["BaseOS", "AppStream", "extras"]

    centos_mirrors = {
        "cs9": [
            f"http://mirror.stream.centos.org/9-stream/",
            f"https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/",
        ],
    }

    packages_list_dir = sys.argv[1]
    upload_prefix = sys.argv[2]
    centos_versions = sys.argv[3:]

    aws_bucket = os.environ.get("AWS_BUCKET_REPOS")
    aws_region = os.environ.get("AWS_REGION")

    repo_dir = os.environ.get("REPO_DIR", "/var/lib/repos")
    clean_repo_dir = os.environ.get("CLEAN_REPO_DIR")

    if not os.path.isabs(repo_dir):
        logging.error(f"REPO_DIR path must be an absolute path: {repo_dir}")
        return 1

    logging.info(f"Creating repositories in: {repo_dir}")
    logging.info(f"Requested centos_versions: {centos_versions}")

    for version in centos_versions:
        logging.info(f"Creating {version} repos")
        if version not in centos_mirrors:
            logging.error(
                f"Unsupported/invalid value provided as parameter CENTOS_VERSION: {version}"
            )
            return 1

        for arch in supported_arches:
            # Download packages for this version/arch combo
            logging.info(f"Downloading packages for {version}/{arch}")
            returnCode = download_packages(
                packages_list_dir,
                version,
                f"{repo_dir}/{version}",
                arch,
                repositories,
                centos_mirrors[version],
            )
            if returnCode:
                return 1
            logging.info(f"Download finished for {version}/{arch}")

            # Generate yum repo index for downloaded packages
            for repo in repositories:
                returnCode = create_repo(f"{repo_dir}/{version}/{repo}/{arch}/os")
                if returnCode:
                    return 1

            # Test the repository on the local filesystem for this version/arch combo
            returnCode = test_local_repo(
                repo_dir=repo_dir, centos_version=version, arch=arch
            )
            if returnCode:
                return 1

        # Show the list of downloaded packages for this version
        packages_downloaded = list(Path(f"{repo_dir}/{version}").rglob("*.rpm"))
        logging.debug(f"Packages downloaded for {version}: {packages_downloaded}")
        logging.info(f"Packages downloaded for {version}: {len(packages_downloaded)}")

        # Upload packages for this version
        if aws_bucket:
            logging.info(
                "Uploading packages (%s) to aws bucket: %s", version, aws_bucket
            )
            returnCode = upload_directory(
                directory=f"{repo_dir}/{version}",
                prefix=f"{upload_prefix}/{version}",
                bucket=aws_bucket,
            )
            if returnCode:
                return 1

            # Test all version/arch combos for this version
            for arch in supported_arches:
                returnCode = test_aws_repo(
                    bucket=aws_bucket,
                    region=aws_region,
                    upload_prefix=upload_prefix,
                    centos_version=version,
                    arch=arch,
                )
                if returnCode:
                    return 1

        # Remove the local files for this version if required
        # CI does this as there is not enough space for multiple versions
        if clean_repo_dir:
            logging.info("Deleting locally downloaded packages")
            try:
                shutil.rmtree(repo_dir)
            except OSError as e:
                print("Error: %s - %s." % (e.filename, e.strerror))

    return 0


if __name__ == "__main__":
    sys.exit(main())
