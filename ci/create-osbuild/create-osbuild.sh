#!/bin/bash
set -euxo pipefail

# following steps as noted on https://hackmd.io/ZbUOkeIXTjaP7XVpTSsrNw?view

# Get OS data.
source /etc/os-release

ID=${ID:-}
ARCH=$(arch)
UUID=${UUID:-local}
CS_VERSION="${CS_VERSION:-8}"

if [[ "${UUID}" != "local" ]]; then
  #TODO: Remove extra single quotes added by Testing Farm due an known bug
  TF_SSH_KEY="${TF_SSH_KEY//\'/}"
fi

BUCKET=${BUCKET:-}
REGION=${REGION:-}
REPO_URL="https://${BUCKET}.s3.${REGION}.amazonaws.com/${UUID}/cs${CS_VERSION}"
DISK_IMAGE=${DISK_IMAGE:-"image_output/image/disk.img"}
IMAGE_FILE=${IMAGE_FILE:-"/var/lib/libvirt/images/auto-osbuild-cs${CS_VERSION}-${ARCH}-${UUID}.raw"}
# allow for a different manifest file to be built - passed via pipeline parameters
PREPROCESSOR_FILE=${PREPROCESSOR_FILE:-$1}
OSBUILT_FILE=ci/create-osbuild/cs${CS_VERSION}-${ARCH}.mpp.json.built
LOCAL_REPO_DIR=${REPO_DIR:-"/var/lib/repos"}/cs${CS_VERSION}

echo ""
echo "[+] Preprocessing $PREPROCESSOR_FILE"
echo ""

if [[ "${UUID}" != "local" ]]; then
  # install osbuild and osbuild-tools, which contains osbuild-mpp utility
  dnf -y copr enable @osbuild/osbuild
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/epel-${CS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/centos-stream-${CS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr\:copr.fedorainfracloud.org\:group_osbuild\:osbuild.repo

  dnf -y install osbuild osbuild-tools osbuild-ostree

  # enable neptune copr repo
  dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${CS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${CS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  # Set the pipeline's yum-repo
  echo "[+] Using the pipeline's yum-repo:"
  echo "repo: ${REPO_URL}"
  osbuild-mpp -D "cs${CS_VERSION}_baseurl=\"${REPO_URL}\"" \
	  -D root_ssh_key="\"${TF_SSH_KEY}\"" \
	  -D ssh_permit_root_login="true" \
	  "$PREPROCESSOR_FILE" "$OSBUILT_FILE"

else
  REVISION="main"

  # enable neptune copr repo
  sudo dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${CS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${CS_VERSION}-\$basearch/"
  sudo sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  osbuild-mpp -D "cs${CS_VERSION}_baseurl=\"${LOCAL_REPO_DIR}\"" \
	  "$PREPROCESSOR_FILE" "$OSBUILT_FILE"
fi

# build the image
sudo osbuild \
	--store osbuild_store \
	--output-directory image_output \
	--export image \
	$OSBUILT_FILE

echo "[+] Moving the generated image"
sudo mkdir -p $(dirname $IMAGE_FILE)
sudo mv $DISK_IMAGE $IMAGE_FILE

# record some details of the input manifest, and disk image in json
cat <<EOF | sudo tee -a ${IMAGE_FILE%.*}.json
{
  "preprocessor_file": "${PREPROCESSOR_FILE}",
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "centos_version": "${CS_VERSION}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF

# Clean up
echo "[+] Cleaning up"
sudo rm -fr image_output osbuild_store

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
echo
