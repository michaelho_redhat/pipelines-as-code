#!/usr/bin/env python3

import argparse
import json
import pathlib
import requests
import sys

default_workload = "workload--automotive-toolchain-<cs>-workload--automotive-toolchain-<cs>-env--automotive-toolchain-<cs>-repositories--<arch>.json"
defaults = {
    "tiny_distro_builder_url": "https://tiny.distro.builders/",
    "cs9": {
        "workload_aarch64": default_workload.replace("<cs>", "c9s").replace(
            "<arch>", "aarch64"
        ),
        "workload_x86": default_workload.replace("<cs>", "c9s").replace(
            "<arch>", "x86_64"
        ),
    },
}


def strip_epoch(version):
    """Strips epoch string from a version string"""
    if ":" in version:
        version = version.split(":")[1]
    return version


def get_packages_from_json(json_file):
    """Extracts set of packages from a content-resolver json output file"""
    packages = set()
    crjson = json.loads(json_file.read_bytes())
    for pkg in crjson["pkg_query"]:
        name = pkg["name"]
        version = strip_epoch(pkg["evr"])
        packages.add(f"{name}-{version}")
    return packages


def main():
    parser = argparse.ArgumentParser(
        description="Convert tiny.distro.builders json to package manifests json"
    )
    parser.add_argument("--distro", type=str, default="cs9", choices=["cs9"])
    parser.add_argument("--aarch64", type=pathlib.Path, required=False)
    parser.add_argument("--x86_64", type=pathlib.Path, required=False)
    args = parser.parse_args()

    crjson_distro = args.distro
    crjson_aarch64_file = args.aarch64
    crjson_x86_file = args.x86_64

    # Sanity check
    if crjson_distro not in defaults:
        raise Exception(f"No default configured for distro {crjson_distro}")

    # Download the inputs from the nightly tiny distro builder job if no file is specified
    default_workload_aarch64 = defaults[crjson_distro]["workload_aarch64"]
    default_workload_x86 = defaults[crjson_distro]["workload_x86"]

    if not crjson_aarch64_file:
        print("Downloading aarch64 workload..", file=sys.stderr)
        resp = requests.get(
            f"{defaults['tiny_distro_builder_url']}{default_workload_aarch64}"
        )
        print(f"Downloaded {resp.url}", file=sys.stderr)
        open(default_workload_aarch64, "wb").write(resp.content)
        crjson_aarch64_file = pathlib.Path(default_workload_aarch64)

    if not crjson_x86_file:
        print("Downloading x86_64 workload..", file=sys.stderr)
        resp = requests.get(
            f"{defaults['tiny_distro_builder_url']}{default_workload_x86}"
        )
        print(f"Downloaded {resp.url}", file=sys.stderr)
        open(default_workload_x86, "wb").write(resp.content)
        crjson_x86_file = pathlib.Path(default_workload_x86)

    # Initiate empty sets
    packages_aarch64 = set()
    packages_x86 = set()

    # Parse the aarch64 json and add to the aarch64 set
    packages_aarch64.update(get_packages_from_json(crjson_aarch64_file))

    # Parse the x86_64 json and add to the x86_64 set
    packages_x86.update(get_packages_from_json(crjson_x86_file))

    # Determine the common and architecture unique packages using set maths
    common = packages_x86.intersection(packages_aarch64)
    x86 = packages_x86.difference(common)
    aarch64 = packages_aarch64.difference(common)

    # Generate the package manifests json file for automotive-sig
    # See: https://gitlab.com/redhat/automotive/automotive-sig/-/blob/main/package_list/cs9-image-manifest.json
    output = {
        "cs9": {
            "common": sorted(list(common), key=str.lower),
            "arch": {
                "x86_64": sorted(list(x86), key=str.lower),
                "aarch64": sorted(list(aarch64), key=str.lower),
            },
        }
    }

    print(json.dumps(output, indent=4))


if __name__ == "__main__":
    main()
