# About

The script gen_package_lists.py can be used to re-create the cs9-image-manifest.json file in the automotive-sig repo.

# Running

You can build and run the docker image with the following commands.

```
podman build -t gen_package_list:latest .
podman run gen_package_list:latest
```

If successful, the script will output a json file to stdout (and some debug messages to stderr).

Note that you cannot run the podman image with -t as this mixes the stderr and stdout content into one stream.

The inputs/outputs are explained in the below sections.

# Inputs

By default the script uses the cs9 results of tiny distro builder (https://tiny.distro.builders/) as input. The script downloads the cs9 automotive-toolchain workloads which contains package definitions for both aarch64 and x86_64.

Tiny distro builder is fed by two workloads defined here by the BE-team:

(1) https://github.com/minimization/content-resolver-input/blob/master/configs/automotive-toolchain-c9s-workload.yaml
    https://github.com/minimization/content-resolver-input/blob/master/configs/automotive-toolchain-c9s-env.yaml

Workload (1) defines the packages needed to run the centos stream 8 build for automotive-sig for both x86_64 and aarch64.

These are consumed nightly by tiny distro builder and run through content-resolver (https://github.com/minimization/content-resolver) to generate the results hosted on the internet.

https://tiny.distro.builders/workload--automotive-toolchain-c9s-workload--automotive-toolchain-c9s-env--automotive-toolchain-c9s-repositories--aarch64.html
https://tiny.distro.builders/workload--automotive-toolchain-c9s-workload--automotive-toolchain-c9s-env--automotive-toolchain-c9s-repositories--x86_64.html

The defaults can also be overriden and it can also be fed two script parameters that redirect it to load the input json files from local files.

```
--aarch64 <local aarch64 file>
--x86_64 <local x86_64 file>
```

Example:
```
wget 'https://tiny.distro.builders/workload--automotive-toolchain-c9s-workload--automotive-toolchain-c9s-env--automotive-toolchain-c9s-repositories--aarch64.json' -O ./aarch64.json
wget 'https://tiny.distro.builders/workload--automotive-toolchain-c9s-workload--automotive-toolchain-c9s-env--automotive-toolchain-c9s-repositories--x86_64.json' -O ./x86_64.json
podman run \
  --privileged \
  -v $(pwd):/mnt \
  gen_package_list \
  --aarch64 /mnt/aarch64.json \
  --x86_64 /mnt/x86_64.json
```

This is useful when running content-resolver locally and testing directly the results.

# Outputs

The script outputs directly to stdout the json file resulting from the two input files.

This outputted file can be used as the cs9-image-manifest.json.
